package com.bobbuy.repository;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.bobbuy.domain.Shop;


@Repository("shopRepository")
public class JpaShopRepository implements ShopRepository {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void save(Shop shop) {
		// TODO Auto-generated method stub
		entityManager.persist(shop);
	}
}
