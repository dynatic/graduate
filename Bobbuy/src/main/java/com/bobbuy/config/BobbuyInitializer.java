package com.bobbuy.config;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

// 부트스트랩 클래스. 이 클래스에서 onStartup으로 부트스트랩한다.
public class BobbuyInitializer implements WebApplicationInitializer {

	// configuration파일 리스트
	private static final Class<?>[] configurationClasses = new Class<?>[] { WebMvcContextConfiguration.class, InfrastructureContextConfiguration.class };

	private static final String DISPATCHER_SERVLET_NAME = "dispatcher";

	// 부트스트랩 메소드. 서버 시작시 필요한 초기화 코드를 구현.
	@Override
	public void onStartup(ServletContext servletContext)
			throws ServletException {
		registerListener(servletContext);
		registerDispatcherServlet(servletContext);
		registerFilter(servletContext);
	}

	// 리스너 등록
	// 1. 디스패쳐 서블릿의 루트 컨텍스트를 생성하고 관계 정의. ContextLoaderListener(rootContext)
	// 2. request를 다른 곳에서 호출 할 수 있게 해주는 리스너 등록. RequestContextListener
	// 아직 확실히 몰라서 넣지만 추후 필요 없으면 삭제 바람.
	private void registerListener(ServletContext servletContext) {
		AnnotationConfigWebApplicationContext rootContext = createContext(configurationClasses);
		servletContext.addListener(new ContextLoaderListener(rootContext));
		servletContext.addListener(new RequestContextListener());
	}

	// 필터 등록
	// 1. CharacterEncodingFilter 등록.
	private void registerFilter(ServletContext servletContext) {
		// TODO Auto-generated method stub
		
		EnumSet<DispatcherType> dispatcherTypes = EnumSet.of(
				DispatcherType.REQUEST, DispatcherType.FORWARD);

		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		characterEncodingFilter.setForceEncoding(true);

		FilterRegistration.Dynamic characterEncoding = servletContext
				.addFilter("characterEncoding", characterEncodingFilter);
		characterEncoding.addMappingForUrlPatterns(dispatcherTypes, true, "/*");
	}

	// DispatcherSevlet을 생성하는 메소드
	// 디스패쳐 콘텍스에 해당하는 설정을 여기서 함.
	private void registerDispatcherServlet(ServletContext servletContext) {
		AnnotationConfigWebApplicationContext dispatcherContext = createContext(WebMvcContextConfiguration.class);
		ServletRegistration.Dynamic dispatcher = servletContext.addServlet(
				DISPATCHER_SERVLET_NAME, new DispatcherServlet(
						dispatcherContext));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/");
	}

	// 콘텍스트 팩토리 클래스
	private AnnotationConfigWebApplicationContext createContext(
			final Class<?>... annotatedClasses) {
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.register(annotatedClasses);
		return context;
	}

}
